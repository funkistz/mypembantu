<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lecturize\Addresses\Traits\HasAddresses;

class Employee extends Model
{
    use HasAddresses, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'display_name',

        'height',
        'weight',
        'marital_status',
        'has_children',

        'religion_id',
        'religion_other',
        'nationality_id',
        'nationality_other',
        'race_id',
        'race_other',

        'start_working_at',
        'speaking_languages',
        'writing_languages',
        'salary_monthly',
        'salary_daily',
        'salary_hourly',
        'about',

        'phone',
        'phone_2',
        'facebook',
        'linkedin',
        'google_plus',
    ];

    protected $dates = ['deleted_at', 'start_working_at'];
    protected $casts = [
        'speaking_languages' => 'array',
        'writing_languages'  => 'array',
    ];

    public function user()
    {
        return $this->morphOne(User::class, 'userable');
    }

    public function nationality()
    {
        return $this->belongsTo(Nationality::class);
    }

    public function getNationalityNameattribute()
    {
        if (!empty($this->attributes['nationality_id'])) {
            return $this->nationality->name;
        } else {
            return '';
        }

    }

    public function race()
    {
        return $this->belongsTo(Race::class);
    }

    public function getRaceNameattribute()
    {
        if (!empty($this->attributes['race_id'])) {
            return $this->race->name;
        } else {
            return '';
        }

    }

    public function religion()
    {
        return $this->belongsTo(Religion::class);
    }

    public function getIsCompleteAttribute()
    {
        if ($this->attributes['is_profile_completed'] == 1) {
            return true;
        } else {
            return false;
        }
    }

}
