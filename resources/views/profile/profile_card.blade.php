<div class="card testimonial-card">

    <!-- Bacground color -->
    <div class="card-up indigo lighten-1">
    </div>

    <!-- Avatar -->
    <div class="avatar mx-auto white">
        <img src="{{ $user->avatarPathThumb }}" class="rounded-circle" style="min-height:110px;">
    </div>

    <div class="card-body">
        <!-- Name -->
        <h5 class="card-title">{{ $user->name }}</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{ $user->email }}</h6>
    </div>

    @if($user->userable_type == 'App\Models\Employee')
    <div class="list-group list-group-flush text-left">
        <a href="{{ route('employee.personal.index') }}" class="list-group-item waves-light">Personal Information</a>
        <a href="{{ route('employee.job_details.index') }}" class="list-group-item waves-light">Job Related Information</a>
        <a href="{{ route('profile.account.index') }}" class="list-group-item waves-light">Change Password</a>
        <a href="{{ route('profile_picture.index') }}" class="list-group-item waves-light">Change Picture</a>
    </div>
    @endif

    @if($user->userable_type == 'App\Models\Employer')
    <div class="list-group list-group-flush text-left">
        <a href="{{ route('employer.personal.index') }}" class="list-group-item waves-light">Personal Information</a>
        <a href="{{ route('employer.job_details.index') }}" class="list-group-item waves-light">Employer Related Information</a>
        <a href="{{ route('profile.account.index') }}" class="list-group-item waves-light">Change Password</a>
        <a href="{{ route('profile_picture.index') }}" class="list-group-item waves-light">Change Picture</a>
    </div>
    @endif

    @if($user->type == 'admin')
    <div class="list-group list-group-flush text-left">
        <a href="{{ route('admin.dashboard.index') }}" class="list-group-item waves-light">Dashboard</a>
        <a href="{{ route('admin.employee.index') }}" class="list-group-item waves-light">Maid</a>
        <a href="{{ route('admin.employer.index') }}" class="list-group-item waves-light">Employer</a>
        <a href="{{ route('profile.account.index') }}" class="list-group-item waves-light">Change Password</a>
        <a href="{{ route('profile_picture.index') }}" class="list-group-item waves-light">Change Picture</a>
    </div>
    @endif

</div>
