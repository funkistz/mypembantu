@if(!$employee->isComplete)
<div class="alert alert-danger" role="alert">
Please complete your profile!
</div>
@endif