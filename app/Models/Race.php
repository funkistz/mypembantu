<?php

namespace App\Models;

use App\Traits\toJsonOption;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Race extends Model
{
    use SoftDeletes, toJsonOption;

    protected $fillable =
    array(
        'code',
        'name',
        'description',
        'is_active',
    );

    protected $dates = ['deleted_at'];
}
