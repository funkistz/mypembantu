@extends('layouts.app2')

@section('content')

@include('employee.alert')

<div class="row">
    <div class="col-md-4">

        @include('profile.profile_card')

    </div>
    <div class="col-md-8">

        <div class="card" style="min-height:100%;">

            <!-- Card content -->
            <div class="card-body text-left">

                <!--begin::Form-->
                <form id="form-job-related-details" action="{{ route('employee.job_details.store') }}" method="post" v-cloak>

                    <!-- Title -->
                    <button type="button" class="btn btn-sm btn-indigo mt-0 float-right" v-on:click="editMode" v-if="!edit">Edit</button>
                    <button type="button" class="btn btn-sm btn-blue-grey mt-0 float-right" v-on:click="viewMode" v-if="edit">Cancel</button>
                    <button type="submit" class="btn btn-sm btn-primary mt-0 float-right" v-if="edit">Save</button>
                    <h4 class="card-title"><a>Job Related Information</a></h4>
                    <hr>

                    @if(isset($method))
                    {{ method_field($method) }}
                    @endif
                    {{ csrf_field() }}

                    <b-form-group horizontal :label-cols="3" label="Working Experience">
                        <input name="name" type="text" class="form-control " placeholder="Not set" value="{{ !empty($employee->start_working_at)? $employee->start_working_at->format('d/m/Y'):null }}" readonly v-if="!edit">
                        <div class="md-form mt-0" :class="(!edit)? 'd-none':''">
                            <input name="start_working_at" value="{{ !empty($employee->start_working_at)? $employee->start_working_at->format('d/m/Y'):null }}" placeholder="Select a date" type="text" class="form-control datepicker">
                        </div>
                        <small class="form-text text-muted" v-if="edit">
                            Specify the date you start working
                        </small>
                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="Speaking Language">
                        <input value="{{ !empty($employee->speaking_languages)? implode(',',$employee->speaking_languages):'' }}" class="form-control" readonly v-if="!edit">
                        <select name="speaking_languages[]" class="mdb-select" multiple :class="(!edit)? 'd-none':''" placeholder="Not set">
                            <option value="" disabled selected>Choose your language</option>
                            <option value="malay">Malay</option>
                            <option value="english">English</option>
                            <option value="chinese">Chinese</option>
                            <option value="tamil">Tamil</option>
                        </select>
                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="Writing Language">
                        <input value="{{ !empty($employee->writing_languages)? implode(',',$employee->writing_languages):'' }}" class="form-control" readonly v-if="!edit">
                        <select name="writing_languages[]" class="mdb-select" multiple :class="(!edit)? 'd-none':''" placeholder="Not set">
                            <option value="" disabled selected>Choose your language</option>
                            <option value="malay">Malay</option>
                            <option value="english">English</option>
                            <option value="chinese">Chinese</option>
                            <option value="tamil">Tamil</option>
                        </select>
                    </b-form-group>

                    <h4 class="card-title"><a>Salary</a></h4>
                    <hr>

                    <b-form-group horizontal :label-cols="2" label="Monthly (RM)">
                        <input name="salary_monthly" type="number" class="form-control" placeholder="RM" value="{{ $employee->salary_monthly or old('salary_monthly') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Daily (RM)">
                        <input name="salary_daily" type="number" class="form-control" placeholder="RM" value="{{ $employee->salary_daily or old('salary_daily') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Hourly (RM)">
                        <input name="salary_hourly" type="number" class="form-control" placeholder="RM" value="{{ $employee->salary_hourly or old('salary_hourly') }}" :readonly="!edit">
                    </b-form-group>


                    <br>

                    <b-form-group horizontal :label-cols="2" label="">
                        <button type="button" class="btn btn-sm btn-blue-grey mt-0 float-right" v-on:click="viewMode" v-if="edit">Cancel</button>
                        <button class="btn btn-sm btn-primary m-0 float-right" v-if="edit">Save</button>
                    </b-form-group>


                </form>

            </div>

        </div>

    </div>
</div>


@endsection

@section('vue')
<script defer>
    const app = new Vue({
        el: '#app',
        data: {
            edit: false,
            start_work_at: null
        },
        methods: {
            editMode: function () {
                this.edit = true;
                $('.mdb-select').removeClass('d-none');
            },
            viewMode: function () {

                var retVal = confirm("Do you want to quit editing? All your change with loss.");
                $('.mdb-select').addClass('d-none');

                if( retVal == true ){
                        this.edit = false;
                }
            }
        }
    });

</script>
@endsection

@push('scripts')
{!! JsValidator::formRequest('App\Http\Requests\JobRelatedDetailsRequest', '#form-job-related-details') !!}
<script>
$('.datepicker').pickadate({
    format: 'dd/mm/yyyy',
    formatSubmit: 'yyyy/mm/dd',
});

$('[name="speaking_languages[]"]').val({!! json_encode($employee->speaking_languages) !!});
$('[name="writing_languages[]"]').val({!! json_encode($employee->writing_languages) !!});
$('.mdb-select').material_select();
</script>
@endpush
