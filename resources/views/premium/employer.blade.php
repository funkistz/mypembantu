@extends('layouts.auth')

@section('nav-content')
<!-- <a class="btn btn-outline-pink btn-md my-2 my-sm-0 ml-3" href="{{ route('login') }}">Sign in</a> -->
@endsection

@section('content')

<!-- Section: Features v.3 -->
<section class="my-5">

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5">Apakah “Ahli Premium” dan berapakah kosnya?</h2>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5 text-center text-lg-left">
      <img  class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/screens-section.jpg" alt="Sample image">
    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3">

        <!-- Grid column -->
        <div class="col-1">
          <i class="fa fa-mail-forward fa-lg indigo-text"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-xl-10 col-md-11 col-10">
          <h5 class="font-weight-bold mb-3">Narrowed your search</h5>
          <p class="grey-text">
            More available filter to easily find your desired maid.
          </p>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row mb-3">

        <!-- Grid column -->
        <div class="col-1">
          <i class="fa fa-mail-forward fa-lg indigo-text"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-xl-10 col-md-11 col-10">
          <h5 class="font-weight-bold mb-3">Exposed your profile</h5>
          <p class="grey-text">
          User does not need to login to view your profile.
          </p>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row mb-3">

        <!-- Grid column -->
        <div class="col-1">
          <i class="fa fa-mail-forward fa-lg indigo-text"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-xl-10 col-md-11 col-10">
          <h5 class="font-weight-bold mb-3">No ad</h5>
          <p class="grey-text">
          No annoying ad to disturb your search
          </p>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

      <!--Grid row-->
      <div class="row">

        <!-- Grid column -->
        <div class="col-1">
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-xl-10 col-md-11 col-10">
            <h5 class="font-weight-bold mb-3"></h5>
            <a href="{{ route('register.employer.create') }}" class="btn btn-warning btn-lg btn-rounded"><i class="fa fa-user left"></i>
                Daftar
            </a>
        </div>
        <!-- Grid column -->

      </div>
      <!--Grid row-->


    </div>
    <!--Grid column-->

  </div>
  <!-- Grid row -->

</section>
<!-- Section: Features v.3 -->


@endsection

@push('scripts')


@endpush
