<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required|min:3|max:60',
            'display_name'   => 'required|min:3|max:60',
            'nationality_id' => 'required',
            'race_id'        => 'required',
            'height'         => 'numeric|min:0|max:300',
            'weight'         => 'numeric|min:0|max:300',
            'street'         => 'required|min:3|max:60',
            'street_extra'   => 'required|min:3|max:60',
            'post_code'      => 'required|numeric',
            'city'           => 'required|min:3|max:60',
            'state'          => 'required|min:3|max:60',
            'country_id'     => 'required',
            'facebook'       => 'max:60',
            'linkedin'       => 'max:60',
            'google_plus'    => 'max:60',
        ];
    }
}
