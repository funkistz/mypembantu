<?php

namespace App\Models;

use App\Traits\toJsonOption;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use toJsonOption;

    protected $fillable =
    array(
        'id',
        'code',
        'name',
        'country_id',
        'description',
        'is_active',
    );

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
