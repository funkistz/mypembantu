@extends('layouts.app2')

@push('css')
<style type="text/css">

  html,
      body,
      header,
      .carousel, .carousel .view {
        height: 60vh;
      }

      @media (max-width: 740px) {
        html,
        body,
        header,
        .carousel {
          height: 100vh;
        }
      }

      @media (min-width: 800px) and (max-width: 850px) {
        html,
        body,
        header,
        .carousel {
          height: 100vh;
        }
      }

      @media (min-width: 800px) and (max-width: 850px) {
              .navbar:not(.top-nav-collapse) {
                  background: #929FBA!important;
              }
          }

</style>
@endpush

@section('header')

    @include('employee.carousel')

@endsection

@section('content')

<div class="row">
    <div class="col-md-3">

        @include('employee.filter')

    </div>

    <div class="col-md-9">

        @include('employee.filter-header')

        <grid-list-view :items="items"></grid-list-view>

    </div>
    <div class="col-lg-9">
    </div>

</div>
@endsection

@section('vue')
<script defer>
    const app = new Vue({
        el: '#app',
        data: {
            items: {!! $employer !!},
        },
        mounted: function () {
            console.log(this.items);
        },
        methods: {
        }
    });

</script>
@endsection
