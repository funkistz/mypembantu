<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Storage Path
    |--------------------------------------------------------------------------
    |
     */
    'master_email'      => env("MASTER_EMAIL"),
    'master_password'   => env("MASTER_PASSWORD"),

    'disk'              => env("DISK"),
    'general_path'      => 'public/' . env("STORAGE_GENERAL"),
    'user_path'         => 'public/' . env("STORAGE_USER"),
    'user_image_path'   => 'public/' . env("STORAGE_USER") . 'images/',

    'image_ext'         => 'png',
    'image_thumb'       => '88',
    'image_medium'      => '200',

    'allowed_mime_type' => ['image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/svg+xml', 'application/octet-stream'],

];
