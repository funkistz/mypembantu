<?php

namespace App\Models;

use App\Traits\toJsonOption;
use Illuminate\Database\Eloquent\Model;

class WorkSkill extends Model
{
    use toJsonOption;

    protected $fillable =
    array(
        'name',
        'description',
        'is_active',
    );

    protected $dates = ['deleted_at'];

    public function getNameAttribute()
    {
        return ucfirst(explode("|", $this->attributes['name'])[0]);
    }

    public function employers()
    {
        return $this->belongsToMany(Employer::class);
    }
}
