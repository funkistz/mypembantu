@extends('layouts.auth')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-7">

        <div class="card card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">
            <div class="text-white text-center py-5 px-4 my-5" style="height:450px;">
                <div>
                    <h1 class="card-title pt-3 mb-5 font-bold"><strong>MyPembantu.com</strong></h1>
                    <p class="mx-5 mb-5"></p>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-5">

        <!--Section: Live preview-->
        <section class="form-light">

            <!--Form without header-->
            <div class="card">

                <div class="card-body mx-4">

                    <!--Header-->
                    <div class="text-center">
                        <h3 class="pink-text mb-5"><strong>Sign In</strong></h3>
                    </div>

                    <form action="/login" method="POST" id="form-login" >

                        {{ csrf_field() }}

                        <!--Body-->
                        <div class="md-form">
                            <input type="text" id="Form-email" class="form-control" name="email">
                            <label for="Form-email">Email</label>
                        </div>

                        <div class="md-form pb-3">
                            <input type="password" id="Form-pass" class="form-control" name="password">
                            <label for="Form-pass">Password</label>
                        </div>

                        <!--Grid row-->
                        <div class="row d-flex align-items-center mb-4 mt-5">

                            <!--Grid column-->
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-pink btn-block btn-rounded z-depth-1">Sign in</button>
                            </div>
                            <!--Grid column-->

                        </div>
                        <!--Grid row-->

                    </form>

                </div>

                <!--Footer-->
                <div class="footer py-3 unique-color-dark">

                    <ul class="list-unstyled list-inline text-center pt-2">
                        <li class="list-inline-item">
                            <h5 class="white-text">Register for free</h5>
                        </li>
                    </ul>
                    <a href="{{ route('register.employer.create') }}" class="btn btn-sm btn-outline-white btn-rounded waves-effect waves-light">Register as Employer</a>
                    <a href="{{ route('register.employee.create') }}" class="btn btn-sm btn-outline-white btn-rounded waves-effect waves-light">Register as Maid</a>
                    <br>
                </div>

            </div>
            <!--/Form without header-->

        </section>
        <!--Section: Live preview-->

    </div>
</div>

@endsection

@push('scripts')

{!! JsValidator::formRequest('App\Http\Requests\LoginRequest', '#form-login') !!}

@endpush
