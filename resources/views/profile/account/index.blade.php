@extends('layouts.app2')

@section('content')

<div class="row">
    <div class="col-md-4">

        @include('profile.profile_card')

    </div>
    <div class="col-md-8">

        <div class="card" style="min-height:100%;">

            <!-- Card content -->
            <div class="card-body text-left">

                <!--begin::Form-->
                <form id="form-change-password" action="{{ route('profile.account.store') }}" method="post" >

                    <button type="button" class="btn btn-sm btn-blue-grey mt-0 float-right" v-on:click="viewMode" v-cloak>Cancel</button>
                    <button type="submit" class="btn btn-sm btn-primary mt-0 float-right" v-cloak>Save</button>
                    <h4 class="card-title"><a>Change Password</a></h4>
                    <hr>

                    @if(isset($method))
                    {{ method_field($method) }}
                    @endif
                    {{ csrf_field() }}

                    <b-form-group horizontal :label-cols="3" label="Name" v-cloak>
                        <input type="text" class="form-control " placeholder="Name" value="{{ $user->name or old('name') }}" readonly>
                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="Email" v-cloak>
                        <input type="text" class="form-control " placeholder="Email" value="{{ $user->email or old('email') }}" readonly>
                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="Current Password" v-cloak>
                        <input name="current_password" type="password" class="form-control " placeholder="Current Password">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="New Password" v-cloak>
                        <input name="password" type="password" class="form-control " placeholder="New Password">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="Re-enter Password" v-cloak>
                        <input name="password_confirmation" type="password" class="form-control " placeholder="Re-enter Password">
                    </b-form-group>

                    <br>

                    <b-form-group horizontal :label-cols="2" label="" v-cloak>
                        <button type="button" class="btn btn-sm btn-blue-grey mt-0 float-right" v-on:click="viewMode">Cancel</button>
                        <button class="btn btn-sm btn-primary m-0 float-right">Save</button>
                    </b-form-group>


                </form>

            </div>

        </div>

    </div>
</div>


@endsection

@section('vue')
<script defer>
    const app = new Vue({
        el: '#app',
    });

</script>
@endsection

@push('scripts')
{!! JsValidator::formRequest('App\Http\Requests\AccountRequest', '#form-change-password') !!}
<script>


</script>
@endpush
