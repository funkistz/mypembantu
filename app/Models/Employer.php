<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lecturize\Addresses\Traits\HasAddresses;

class Employer extends Model
{
    use HasAddresses, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'display_name',

        'height',
        'weight',
        'marital_status',
        'has_children',

        'religion_id',
        'religion_other',
        'nationality_id',
        'nationality_other',
        'race_id',
        'race_other',

        'location_type',
        'employee_types',
        'employee_types_other',
        'work_skill_others',
        'households',
        'job',
        'salary_monthly',
        'salary_daily',
        'salary_hourly',
        'about',

        'phone',
        'phone_2',
        'facebook',
        'linkedin',
        'google_plus',
    ];

    protected $dates = ['deleted_at'];
    protected $casts = [
        'employee_types'    => 'array',
        'work_skill_others' => 'array',
        'households'        => 'json',
    ];

    public function user()
    {
        return $this->morphOne(User::class, 'userable');
    }

    public function workskills()
    {
        return $this->belongsToMany(WorkSkill::class);
    }

    public function getIsCompleteAttribute()
    {
        if ($this->attributes['is_profile_completed'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getLocationTypeNameAttribute()
    {
        if ($this->attributes['location_type'] == 'h') {
            return 'Home';
        } else if ($this->attributes['location_type'] == 's') {
            return 'shop';
        } else if ($this->attributes['location_type'] == 'o') {
            return 'Office';
        } else {
            return 'not set';
        }
    }

    public function getEmployeeTypesNameAttribute()
    {
        if (!empty($this->attributes['employee_types'])) {

            $employee_types = json_decode($this->attributes['employee_types']);
            $types          = [];

            foreach ($employee_types as $type) {

                if ($type == 'ft1') {

                    array_push($types, 'Fulltime (live together)');

                } else if ($type == 'ft2') {

                    array_push($types, 'Fulltime (not live together. shuttle everyday)');

                } else if ($type == 'pt') {

                    array_push($types, 'Parttime (not everyday)');

                }

            }

            return $types;

        } else {
            return 'not set';
        }

    }
}
