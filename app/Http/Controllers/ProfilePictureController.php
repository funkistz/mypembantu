<?php

namespace App\Http\Controllers;

use App\Libraries\FileFolderLibrary;
use App\Models\ProfilePicture;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfilePictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user     = auth()->user();
        $employee = $user->userable;

        $data = [
            'user'     => $user,
            'employee' => $employee,
        ];

        return view('profile.profile_picture')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request['blob'];
        $user = auth()->user();

        $path = config('b3.user_path') . $user->id . '/images/';

        if (!empty($file)) {
            $upload_data = FileFolderLibrary::uploadImage('profile', $path, $file, ['thumb' => true]);

            if (!empty($upload_data)) {

                $path = $path . $upload_data['name'];

                $profile_picture = ProfilePicture::where([
                    ['user_id', $user->id],
                    ['is_primary', 1],
                ])->first();

                if (!empty($profile_picture)) {

                    $profile_picture->update([
                        'name'       => $upload_data['name'],
                        'path'       => $path,
                        'updated_at' => Carbon::now(),
                    ]);

                } else {

                    ProfilePicture::create([
                        'user_id'    => $user->id,
                        'name'       => $upload_data['name'],
                        'path'       => $path,
                        'is_primary' => true,
                    ]);
                }
            }

        } else {

            return response()->json([
                'status'  => 'error',
                'message' => 'Image not found',
            ]);

        }

        return response()->json([
            'status'  => 'success',
            'message' => 'Image successfully change',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
