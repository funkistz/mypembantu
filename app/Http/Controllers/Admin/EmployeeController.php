<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Employee;
use App\Models\Nationality;
use App\Models\Race;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();

        $data = [
            // 'breadcrumb' => [
            //     ['Admin', '#'],
            //     ['Maid', '#'],
            // ],
            'employees' => $employees,
        ];

        return view('admin.employee.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);

        $detail = [
            'name'           => $employee->user->name,
            'display name'   => $employee->display_name,
            'height'         => $employee->height . 'cm',
            'weight'         => $employee->weight . 'kg',
            'marital status' => $employee->marital_status,
            'has children'   => $employee->has_children,
            'nationality'    => $employee->nationality_name,
            'race'           => $employee->race_name,
        ];

        $data = [
            'employee' => $employee,
            'details'  => $detail,
        ];

        return view('admin.employee.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee      = Employee::find($id);
        $countries     = Country::toJsonOption();
        $nationalities = Nationality::toJsonOption();
        $races         = Race::toJsonOption();
        $states        = State::toJsonOption('name', 'name');
        $address       = $employee->getPrimaryAddress();

        $data = [
            // 'breadcrumb'    => [
            //     ['Admin', route('admin.employee.index')],
            //     ['Maid', route('admin.employee.index')],
            //     ['Update', '#'],
            // ],
            'action'        => route('admin.employee.update', $id),
            'method'        => 'PUT',
            'employee'      => $employee,
            'address'       => $address,
            'countries'     => $countries,
            'nationalities' => $nationalities,
            'races'         => $races,
            'states'        => $states,
        ];

        return view('admin.employee.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $employee = Employee::find($id);
        $user     = $employee->user;

        $user->update(
            [
                'name' => $request['name'],
            ]
        );

        $employee->update(
            [
                'display_name'   => $request['display_name'],
                'nationality_id' => $request['nationality_id'],
                'race_id'        => $request['race_id'],
                'facebook'       => $request['facebook'],
                'linkedin'       => $request['linkedin'],
                'google_plus'    => $request['google_plus'],
            ]
        );

        $address = [
            'street'       => $request['street'],
            'street_extra' => $request['street_extra'],
            'state'        => $request['state'],
            'city'         => $request['city'],
            'post_code'    => $request['post_code'],
            'country_id'   => $request['country_id'],
            'is_primary'   => true,
        ];

        if ($employee->hasAddress()) {
            $current_address = $employee->addresses()->first();
            $employee->updateAddress($current_address, $address);
        } else {
            $employee->addAddress($address);
        }

        \Session::flash('alert-success', 'Employee Updated');
        return redirect()->route('admin.employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::find($id)->delete();

        return response()->json([
            'success' => 'Delete successful',
        ]);
    }

    public function verify($id)
    {
        $employee = Employee::find($id);
        $user     = $employee->user;

        $user->verified = Carbon::now();
        $user->save();

        return response()->json([
            'success' => 'Maid verified',
        ]);
    }

    public function unverify($id)
    {
        $employee = Employee::find($id);
        $user     = $employee->user;

        $user->verified = null;
        $user->save();

        return response()->json([
            'success' => 'Maid unverified',
        ]);
    }
}
