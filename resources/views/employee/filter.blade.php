<!-- Card -->
<div class="card">

    <!-- Card body -->
    <div class="card-body">

        <!-- Material form register -->
        <form>

            <b-form-group label="Location:">
                <b-form-select
                            :options="[]"
                            required>
                </b-form-select>
            </b-form-group>

            <b-form-group label="Location type:">
                <b-form-select
                            :options="[]"
                            required>
                </b-form-select>
            </b-form-group>

            <b-form-group label="Skill:">
                <b-form-select
                            :options="[]"
                            required>
                </b-form-select>
            </b-form-group>

            <div class="text-center py-4 mt-3">
                <button class="btn btn-cyan" type="submit">Search</button>
            </div>
        </form>
        <!-- Material form register -->

    </div>
    <!-- Card body -->

</div>
<!-- Card -->
