<?php

namespace App\Models;

use App\Traits\toJsonOption;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use toJsonOption;
}
