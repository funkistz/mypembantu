<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JobDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user     = auth()->user();
        $language = [
            ['value' => 'malay', 'text' => 'Malay'],
            ['value' => 'english', 'text' => 'English'],
            ['value' => 'chinese', 'text' => 'Chinese'],
            ['value' => 'tamil', 'text' => 'Tamil'],
        ];

        // dd(json_encode($language));

        $data = [
            'user'     => $user,
            'employee' => $user->userable,
            'language' => json_encode($language),
        ];

        return view('employee.job_details')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $user                               = auth()->user();
        $employee                           = $user->userable;
        $request['start_working_at_submit'] = Carbon::createFromFormat('Y/m/d', $request['start_working_at_submit']);

        $employee->update(
            [
                'start_working_at'   => $request['start_working_at_submit'],
                'speaking_languages' => $request['speaking_languages'],
                'writing_languages'  => $request['writing_languages'],
                'salary_monthly'     => $request['salary_monthly'],
                'salary_daily'       => $request['salary_daily'],
                'salary_hourly'      => $request['salary_hourly'],
            ]
        );

        \Session::flash('alert-success', 'Job Related Information Updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
