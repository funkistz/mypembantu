<li class="mt-5">
    <ul class="collapsible collapsible-accordion">
        <li class="menu-item">
            <a class="collapsible-header waves-effect" href="{{ route('admin.dashboard.index') }}">
                <i class="fa fa-dashboard"></i>Dashboard
            </a>
        </li>
        <li class="menu-item">
            <a class="collapsible-header waves-effect" href="{{ route('admin.employee.index') }}">
                <i class="fa fa-users"></i>Maid
            </a>
        </li>
        <li class="menu-item">
            <a class="collapsible-header waves-effect" href="{{ route('admin.employer.index') }}">
                <i class="fa fa-user-circle"></i>Employer
            </a>
        </li>
    </ul>
</li>
