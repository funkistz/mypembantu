@extends('layouts.app2')

@section('content')

@include('employer.alert')

<div class="row">
    <div class="col-md-4">

        @include('profile.profile_card')

    </div>
    <div class="col-md-8">

        <div class="card" style="min-height:100%;">

            <!-- Card content -->
            <div class="card-body text-left">

                <!--begin::Form-->
                <form id="form-job-related-details" action="{{ route('employer.job_details.store') }}" method="post" v-cloak>

                    <!-- Title -->
                    <button type="button" class="btn btn-sm btn-indigo mt-0 float-right" v-on:click="editMode" v-if="!edit">Edit</button>
                    <button type="button" class="btn btn-sm btn-blue-grey mt-0 float-right" v-on:click="viewMode" v-if="edit">Cancel</button>
                    <button type="submit" class="btn btn-sm btn-primary mt-0 float-right" v-if="edit">Save</button>
                    <h4 class="card-title"><a>Employer Related Information</a></h4>
                    <hr>

                    @if(isset($method))
                    {{ method_field($method) }}
                    @endif
                    {{ csrf_field() }}

                    <b-form-group horizontal :label-cols="3" label="About">
                        <textarea name="about" class="form-control" :readonly="!edit">{{ $employer->about or old('about') }}</textarea>
                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="Location Type">
                        <input value="{{ $employer->location_type_name or old('location_type') }}" class="form-control" readonly v-if="!edit">
                        <select name="location_type" class="mdb-select" :class="(!edit)? 'd-none':''" placeholder="Not set">
                            <option value="" disabled selected>Choose location type</option>
                            <option value="h">House</option>
                            <option value="s">Shop</option>
                            <option value="o">Office</option>
                        </select>
                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="Household(s)">
                        <button type="button" class="btn btn-sm btn-default" v-if="edit" v-on:click="addHousehold">
                            <i class="fa fa-plus"></i>
                        </button>

                        <div class="form-group row" v-for="(household, key) in households">
                            <label class="col-sm-3 col-form-label">@{{ key+1 }}. Age<span v-if="!edit">: @{{ households[key] }}</span></label>
                            <div class="col-sm-6" v-if="edit">
                                <div class="input-group input-group-sm">
                                    <input name="households[]" type="text" class="form-control" v-model="households[key]">
                                    <div class="input-group-append">
                                        <button class="btn btn-danger" type="button" v-on:click="removeHousehold(key)">Remove</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="Maid type">

                        @foreach( $employer->employee_types_name as $employee_type )
                        <input value="{{ $employee_type }}" class="form-control" readonly v-if="!edit" {{ !empty($employee->has_children)? 'checked':null }}>
                        @endforeach

                        <select name="employee_types[]" class="mdb-select" multiple :class="(!edit)? 'd-none':''" placeholder="Not set">
                            <option value="" disabled selected>Choose maid type</option>
                            <option value="ft1">Fulltime (live together)</option>
                            <option value="ft2">Fulltime (not live together. Shuttle everyday)</option>
                            <option value="pt">Parttime (not everyday)</option>
                        </select>
                    </b-form-group>

                    <b-form-group horizontal :label-cols="3" label="Other maid type">
                        <input name="employee_types_other" value="{{ $employer->employee_types_other or old('employee_types_other') }}" class="form-control" placeholder="specify" :readonly="!edit">
                    </b-form-group>

                    <h4 class="card-title"><a>Skills</a></h4>
                    <hr>

                    @foreach($work_skills as $skill)
                    <b-form-group horizontal :label-cols="8" label="{{ $skill->name }}">
                        <div class="switch" v-if="edit">
                            <label>
                                <input name="skills[{{ $skill->id }}]" type="checkbox" {{ in_array($skill->id, $employee_work_skills)? 'checked':null }} >
                                <span class="lever"></span>
                            </label>
                        </div>
                        <input class="form-control" value="{{ in_array($skill->id, $employee_work_skills)? 'Yes':'No' }}" readonly v-if="!edit">
                    </b-form-group>
                    @endforeach

                    <h4 class="card-title"><a>Salary</a></h4>
                    <hr>

                    <b-form-group horizontal :label-cols="2" label="Monthly (RM)">
                        <input name="salary_monthly" type="number" class="form-control" placeholder="RM" value="{{ $employer->salary_monthly or old('salary_monthly') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Daily (RM)">
                        <input name="salary_daily" type="number" class="form-control" placeholder="RM" value="{{ $employer->salary_daily or old('salary_daily') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Hourly (RM)">
                        <input name="salary_hourly" type="number" class="form-control" placeholder="RM" value="{{ $employer->salary_hourly or old('salary_hourly') }}" :readonly="!edit">
                    </b-form-group>


                    <br>

                    <b-form-group horizontal :label-cols="2" label="">
                        <button type="button" class="btn btn-sm btn-blue-grey mt-0 float-right" v-on:click="viewMode" v-if="edit">Cancel</button>
                        <button class="btn btn-sm btn-primary m-0 float-right" v-if="edit">Save</button>
                    </b-form-group>


                </form>

            </div>

        </div>

    </div>
</div>


@endsection

@section('vue')
<script defer>
    const app = new Vue({
        el: '#app',
        data: {
            edit: false,
            households: {!! json_encode($employer->households) !!}
        },
        mounted: {

        },
        methods: {
            editMode: function () {
                this.edit = true;
                $('.mdb-select').removeClass('d-none');
            },
            viewMode: function () {

                var retVal = confirm("Do you want to quit editing? All your change with loss.");
                $('.mdb-select').addClass('d-none');

                if( retVal == true ){
                        this.edit = false;
                }
            },
            addHousehold: function(){
                this.households.push('');
            },
            removeHousehold: function(key){
                this.households.splice(key, 1);
            }
        }
    });

</script>
@endsection

@push('scripts')
{!! JsValidator::formRequest('App\Http\Requests\JobRelatedDetailsRequest', '#form-job-related-details') !!}
<script>
$('.datepicker').pickadate({
    format: 'dd/mm/yyyy',
    formatSubmit: 'yyyy/mm/dd',
});

$('[name="location_type"]').val('{{ $employer->location_type }}');
$('[name="employee_types[]"]').val({!! json_encode($employer->employee_types) !!});
$('.mdb-select').material_select();
</script>
@endpush
