<?php

namespace App\Traits;

trait toJsonOption
{

    public static function toJsonOption($text = 'name', $value = 'id')
    {
        $data = parent::get([$text . ' AS text', $value . ' As value'])->toArray();

        array_unshift($data, ['text' => '', 'value' => '']);

        return json_encode($data);
    }
}
