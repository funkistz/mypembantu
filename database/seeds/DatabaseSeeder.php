<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesSeeder::class);
        $this->call(NationalitySeeder::class);
        $this->call(RaceSeeder::class);
        $this->call(StateSeeder::class);
        $this->call(WorkSkillSeeder::class);
    }
}
