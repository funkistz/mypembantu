<?php

use App\Models\Race;
use Illuminate\Database\Seeder;

class RaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Race::truncate();
        DB::table('races')->insert([
            [
                'code'        => 'm',
                'name'        => 'Malay',
                'description' => "",
                'is_active'   => 1,
                'created_by'  => 1,
                'created_at'  => date('Y-m-d H:i:s'),
            ],
            [
                'code'        => 'c',
                'name'        => 'Chinese',
                'description' => "",
                'is_active'   => 1,
                'created_by'  => 1,
                'created_at'  => date('Y-m-d H:i:s'),
            ],
            [
                'code'        => 'i',
                'name'        => 'Indian',
                'description' => "",
                'is_active'   => 1,
                'created_by'  => 1,
                'created_at'  => date('Y-m-d H:i:s'),
            ],
            [
                'code'        => 'bsb',
                'name'        => 'Bumiputra Sabah',
                'description' => "",
                'is_active'   => 1,
                'created_by'  => 1,
                'created_at'  => date('Y-m-d H:i:s'),
            ],
            [
                'code'        => 'bsr',
                'name'        => 'Bumiputra Sarawak',
                'description' => "",
                'is_active'   => 1,
                'created_by'  => 1,
                'created_at'  => date('Y-m-d H:i:s'),
            ],
            [
                'code'        => 'o',
                'name'        => 'Others',
                'description' => "",
                'is_active'   => 1,
                'created_by'  => 1,
                'created_at'  => date('Y-m-d H:i:s'),
            ],
        ]);
        $this->command->info('Race Table Seeded');
    }
}
