<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('display_name')->nullable();
            $table->tinyInteger('is_profile_completed');
            $table->integer('premium_id')->nullable();

            $table->integer('height')->nullable();
            $table->integer('weight')->nullable();
            $table->tinyInteger('marital_status')->nullable();
            $table->tinyInteger('has_children')->nullable();

            $table->integer('religion_id')->nullable();
            $table->string('religion_other')->nullable();
            $table->integer('nationality_id')->nullable();
            $table->string('nationality_other')->nullable();
            $table->integer('race_id')->nullable();
            $table->string('race_other')->nullable();

            $table->char('location_type', 2)->nullable();
            $table->json('employee_types')->nullable();
            $table->string('employee_types_other')->nullable();
            $table->json('work_skill_others')->nullable();
            $table->json('households')->nullable();
            $table->string('job')->nullable();

            $table->decimal('salary_monthly', 19, 4)->nullable();
            $table->decimal('salary_daily', 19, 4)->nullable();
            $table->decimal('salary_hourly', 19, 4)->nullable();

            $table->text('about')->nullable();

            $table->string('phone')->nullable();
            $table->string('phone_2')->nullable();
            $table->string('facebook')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('google_plus')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employers');
    }
}
