<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Models\Country;
use App\Models\Employee;
use App\Models\Nationality;
use App\Models\Race;
use App\Models\State;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user     = auth()->user();
        $employee = $user->userable;

        if (empty($employee)) {
            $employee                       = new Employee();
            $employee->is_profile_completed = false;
            $employee->save();

            $user->userable_id   = $employee->id;
            $user->userable_type = Employee::class;
            $user->save();
        }

        $address = $employee->getPrimaryAddress();

        $countries      = Country::toJsonOption();
        $nationalities  = Nationality::toJsonOption();
        $races          = Race::toJsonOption();
        $states         = State::toJsonOption('name', 'name');
        $marital_status = json_encode([
            ['text' => 'Never married', 'value' => 1],
            ['text' => 'Married', 'value' => 2],
            ['text' => 'Divorced', 'value' => 3],
            ['text' => 'Separated', 'value' => 4],
            ['text' => 'Widowed', 'value' => 5],
        ]);

        $data = [
            'user'           => $user,
            'employee'       => $employee,
            'address'        => $address,
            'countries'      => $countries,
            'nationalities'  => $nationalities,
            'races'          => $races,
            'states'         => $states,
            'marital_status' => $marital_status,
        ];

        return view('profile.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
        // dd($request->all());

        $user                    = auth()->user();
        $employee                = $user->userable;
        $request['has_children'] = ($request['has_children'] == 'on') ? true : false;

        $user->update(
            [
                'name' => $request['name'],
            ]
        );

        $employee->update(
            [
                'display_name'   => $request['display_name'],
                'nationality_id' => $request['nationality_id'],
                'race_id'        => $request['race_id'],
                'height'         => $request['height'],
                'weight'         => $request['weight'],
                'marital_status' => $request['marital_status'],
                'has_children'   => $request['has_children'],

                'facebook'       => $request['facebook'],
                'linkedin'       => $request['linkedin'],
                'google_plus'    => $request['google_plus'],
            ]
        );

        $address = [
            'street'       => $request['street'],
            'street_extra' => $request['street_extra'],
            'state'        => $request['state'],
            'city'         => $request['city'],
            'post_code'    => $request['post_code'],
            'country_id'   => $request['country_id'],
            'is_primary'   => true,
        ];

        if ($employee->hasAddress()) {
            $current_address = $employee->addresses()->first();
            $employee->updateAddress($current_address, $address);
        } else {
            $employee->addAddress($address);
        }

        \Session::flash('alert-success', 'Personal Information Updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
