@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-3">

        @include('admin.navigation')

    </div>
    <div class="col-md-6">

        <div class="card testimonial-card">

            <!-- Bacground color -->
            <div class="card-up green lighten-1">
            </div>

            <!-- Avatar -->
            <div class="avatar mx-auto white">
                <img src="{{ $employee->user->avatarPathThumb }}" class="rounded-circle" style="min-height:110px;">
            </div>

            <div class="card-body">
                <!-- Name -->
                <h5 class="card-title">{{ $employee->user->name }}</h5>
                <h6 class="card-subtitle text-muted">{{ $employee->user->email }}</h6>
                <h6 class="card-subtitle mb-2 mt-2">
                    Facebook: <a href="{{ $employee->facebook or '#' }}" target="_blank" >{{ $employee->facebook or 'not available' }}</a>
                </h6>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <table class="table table-sm table-borderless">
                        <tbody>
                            @foreach( $details as $key => $data )
                            <tr>
                                <td class="text-right text-capitalize" width="30%">{{ $key }} :</td>
                                <td class="text-left">{{ $data }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="p-2">
            @if( !$employee->user->is_verified )
                <btn-icon class="btn-lg btn-block" btn-size="lg" btn-type="warning" href="{{ route('admin.employee.verify', $employee->id) }}"
                ajax="PUT" confirm="Verify" reload="true">Verify</btn-icon>
            @else
                <btn-icon class="btn-lg btn-block" btn-size="lg" btn-type="danger" href="{{ route('admin.employee.unverify', $employee->id) }}"
                ajax="PUT" confirm="Unverify" reload="true">Unverify</btn-icon>
            @endif
            </div>

        </div>


    </div>
</div>

@endsection

@section('vue')
<script defer>
$( document ).ready(function() {
    const app = new Vue({
        el: '#app',
    });
});

</script>
@endsection

@push('scripts')
<script>

</script>

@endpush
