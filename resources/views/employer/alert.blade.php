@if(!$user->is_activated)
<div class="alert alert-danger" role="alert">
Your account is not activate yet, please wait until admin activate your account.
</div>
@endif

@if(!$employer->isComplete)
<div class="alert alert-danger" role="alert">
Please complete your profile!
</div>
@endif
