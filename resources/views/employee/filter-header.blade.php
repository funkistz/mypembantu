<nav class="navbar navbar-expand-lg mt-1 mb-5">

    <!-- Navbar brand -->
    <h5 class="my-2 mr-2" v-cloak>@{{ items.length }} maid found</h5>

    <!-- Collapse button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">

    </ul>

    <b-form inline v-cloak>
      <label class="mr-sm-2">Sort by:</label>
      <b-form-select class="mb-2 mr-sm-2 mb-sm-0"
                     :value="null"
                     :options="{ '1': 'One', '2': 'Two', '3': 'Three' }">
        <option slot="first" :value="null">Choose...</option>
      </b-form-select>
      <button type="button" class="btn btn-flat text-white"><i class="fa fa-list"></i></button>
      <button type="button" class="btn btn-flat text-white"><i class="fa fa-th-large"></i></button>
    </b-form>

    </div>
    <!-- Collapsible content -->

</nav>
