<!-- SideNav slide-out button -->
<!-- <div class="float-left d-block d-xs-none">
    <a href="#" data-activates="slide-out" class="button-collapse black-text"><i class="fa fa-bars"></i></a>
</div> -->

@if(empty($sidebar))
<a class="ml-2 navbar-brand" href="{{ route('home') }}">
    <img src="{{ asset('img/logo.png') }}" height="30" alt="">
</a>
@else
<a class="ml-2 navbar-brand ml-5" href="{{ route('home') }}">
    <img src="{{ asset('img/logo.png') }}" height="30" alt="">
</a>
@endif

@if(!auth()->check())
<div class="form-inline my-2 my-lg-0 ml-auto">
    <a class="btn btn-outline-pink btn-md my-2 my-sm-0 ml-3" href="{{ route('login') }}">Sign in</a>
</div>
@endif
@if(auth()->check())
<ul class="nav navbar-nav nav-flex-icons ml-auto">
    <li class="nav-item">
        <a class="nav-link waves-effect waves-light">1
            <i class="fa fa-bell"></i>
        </a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-user"></i> <span class="clearfix d-none d-sm-inline-block">Account</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">

            @if(auth()->user()->userable_type == 'App\Models\Employee')
            <a class="dropdown-item" href="{{ route('employee.personal.index') }}">Profile</a>
            @elseif(auth()->user()->userable_type == 'App\Models\Employer')
            <a class="dropdown-item" href="{{ route('employer.personal.index') }}">Profile</a>
            @endif

            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
        </div>
    </li>
</ul>
@endif
