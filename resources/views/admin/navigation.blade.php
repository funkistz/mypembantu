<div class="card testimonial-card">

    <!-- Bacground color -->
    <div class="card-up indigo lighten-1">
    </div>

    <!-- Avatar -->
    <div class="avatar mx-auto white">
        <img src="{{ auth()->user()->avatarPathThumb }}" class="rounded-circle" style="min-height:110px;">
    </div>

    <div class="card-body">
        <!-- Name -->
        <h5 class="card-title">{{ auth()->user()->name }}</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{ auth()->user()->email }}</h6>
    </div>

    <div class="list-group list-group-flush text-left">
        <a href="{{ route('admin.dashboard.index') }}" class="list-group-item waves-light">Dashboard</a>
        <a href="{{ route('admin.employee.index') }}" class="list-group-item waves-light">Maid</a>
        <a href="{{ route('admin.employer.index') }}" class="list-group-item waves-light">Employer</a>
        <a href="{{ route('profile.account.index') }}" class="list-group-item waves-light">Change Password</a>
        <a href="{{ route('profile_picture.index') }}" class="list-group-item waves-light">Change Picture</a>
    </div>

</div>
