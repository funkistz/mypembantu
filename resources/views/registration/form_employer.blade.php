@extends('layouts.auth')

@section('nav-content')
<!-- <a class="btn btn-outline-pink btn-md my-2 my-sm-0 ml-3" href="{{ route('login') }}">Sign in</a> -->
@endsection

@section('content')

<div class="row justify-content-center">
    <div class="col-md-5">

        <!--Section: Live preview-->
        <section class="form-light mb-5">

            <!--Form without header-->
            <div class="card">

                <div class="card-body mx-4">

                    <!--Header-->
                    <div class="text-center">
                        <h3 class="pink-text mb-5"><strong>Register as employer</strong></h3>
                    </div>

                    <form action="{{ route('register.employer.store') }}" method="POST" id="form-register" >

                        {{ csrf_field() }}

                        <!--Body-->
                        <div class="md-form">
                            <input type="text" id="Form-name" class="form-control" name="name">
                            <label for="Form-name">Name</label>
                        </div>

                        <div class="md-form">
                            <input type="text" id="Form-email" class="form-control" name="email">
                            <label for="Form-email">Email</label>
                        </div>

                        <div class="md-form">
                            <input type="password" id="Form-pass" class="form-control" name="password">
                            <label for="Form-pass">Your password</label>
                        </div>

                        <div class="md-form pb-3">
                            <input type="password" id="Form-pass2" class="form-control" name="password_confirmation">
                            <label for="Form-pass2">Confirm password</label>
                        </div>

                        <!--Grid row-->
                        <div class="row d-flex align-items-center mb-4 mt-5">

                            <!--Grid column-->
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-pink btn-block btn-rounded z-depth-1">Daftar</button>
                            </div>
                            <!--Grid column-->

                        </div>
                        <!--Grid row-->

                    </form>

                </div>

                <!--Footer-->
                <div class="footer pt-3 unique-color-dark">

                    <ul class="list-unstyled list-inline text-center py-2">
                        <li class="list-inline-item">
                            <h5 class="mb-1 white-text">Sudah mempunyai akaun?</h5>
                        </li>
                        <li class="list-inline-item">
                        <a href="{{ route('login') }} " class="btn btn-outline-white btn-rounded waves-effect waves-light">Daftar Masuk</a>
                        </li>
                    </ul>

                </div>


            </div>
            <!--/Form without header-->

        </section>
        <!--Section: Live preview-->

    </div>
    <div class="col-md-7">

        <div class="card card-image mb-5" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">
            <div class="text-center py-5 px-4 my-5">
                <div>
                    <h1 class="card-title mb-4 font-bold"><strong>Tahukah anda?</strong></h1>
                    <p class="mx-5 mb-4 font-bold">
                    Kos yang anda perlu bayar untuk mendapatkan maid?
                    Ada yang mencecah sehingga ribuan ringgit.
                    Tapi di sini, biar kami yang belanja hingga puluhan ribu ringgit untuk membuat promosi.
                    Anda hanya perlu lihat senarai pembantu yang ada dan jika ada yang anda berkenan, anda boleh upgrade akaun anda kepada “Ahli Premium” untuk mula menghubungi mereka.
                    </p>
                    <a href="{{ route('premium.employer') }}" class="btn btn-warning btn-lg btn-rounded"><i class="fa fa-star left"></i>
                    Apakah ahli premium?</a>

                    <h1 class="card-title pt-5 mb-5 font-bold"><strong>Bagaimana portal ini berfungsi?</strong></h1>

                    <!-- Grid column -->
                    <div class="col-md-12">

                    <!-- Grid row -->
                    <div class="row mb-3">

                        <!-- Grid column -->
                        <div class="col-2">
                        <i class="fa fa-2x fa-sign-in"></i>
                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-10">
                        <h5 class="font-weight-bold mb-3">Login</h5>
                        <p class="black-text">Login untuk menghubungi calon yang anda kehendaki. </p>
                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="row mb-3">

                        <!-- Grid column -->
                        <div class="col-2">
                        <i class="fa fa-2x fa-search"></i>
                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-10">
                        <h5 class="font-weight-bold mb-3">Mula mencari pembantu</h5>
                        <p class="black-text">Admin akan menjadi pengantara perbincangan di antara majikan dan bakal pembantu rumah.</p>
                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="row mb-3">

                        <!-- Grid column -->
                        <div class="col-2">
                        <i class="fa fa-2x fa-check-circle"></i>
                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-10">
                        <h5 class="font-weight-bold mb-3">Persetujuan</h5>
                        <p class="black-text mb-md-0">
                        Jika kedua-dua pihak serasi dan bersetuju untuk bekerjasama, majikan perlu menjelaskan bayaran yang ditetapkan kepada admin sebelum majikan dan bakal pembantu rumah diberikan maklumat lengkap untuk berhubung di antara satu sama lain. Bayaran ini akan disertakan dengan jaminan perkhidmatan.
                        </p>
                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="row mb-md-0 mb-3">

                        <!-- Grid column -->
                        <div class="col-2">
                        <i class="fa fa-2x fa-credit-card"></i>
                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-10">
                        <h5 class="font-weight-bold mb-3">Pembayaran</h5>
                        <p class="black-text mb-md-0">
                        Klik di sini untuk melihat maklumat lanjut tentang bayaran yang dikenakan dan jaminan yang disertakan.
                        </p>
                        <a href="#" class="btn btn-warning btn-lg btn-rounded mt-3">
                            <i class="fa fa-star left"></i>
                            Info pembayaran
                        </a>
                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->

                    </div>
                    <!-- Grid column -->

                </div>
            </div>
        </div>

    </div>
</div>


@endsection

@push('scripts')

{!! JsValidator::formRequest('App\Http\Requests\RegisterRequest', '#form-register') !!}

@endpush
