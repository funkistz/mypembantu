<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/premium/employer', 'HomeController@employerPremium')->name('premium.employer');

Route::group(['middleware' => ['guest.redirect']], function () {

    // registration
    Route::get('/register/maid', 'RegisterEmployeeController@create')->name('register.employee.create');
    Route::post('/register/maid', 'RegisterEmployeeController@store')->name('register.employee.store');

    Route::get('/register/employer', 'RegisterEmployerController@create')->name('register.employer.create');
    Route::post('/register/employer', 'RegisterEmployerController@store')->name('register.employer.store');

    Route::group(['prefix' => 'employee'], function () {

        Route::resource('search', 'Employee\SearchController', ['as' => 'employee']);

    });

});

// please add route that need to login below
// please use resourcefull route for standard
Route::group(['middleware' => ['auth']], function () {

    Route::group(['prefix' => 'user'], function () {

        Route::resource('profile_picture', 'ProfilePictureController');
        Route::resource('account', 'AccountController', ['as' => 'profile']);

    });

    Route::group(['prefix' => 'employee'], function () {

        Route::resource('personal', 'Employee\PersonalInformationController', ['as' => 'employee']);
        Route::resource('job_details', 'Employee\JobDetailsController', ['as' => 'employee']);
    });

    Route::group(['prefix' => 'employer'], function () {

        Route::resource('personal', 'Employer\PersonalInformationController', ['as' => 'employer']);
        Route::resource('job_details', 'Employer\JobDetailsController', ['as' => 'employer']);
    });

    Route::resource('user', 'UserController');

    Route::group(['middleware' => ['admin'], 'prefix' => 'admin'], function () {

        Route::resource('dashboard', 'Admin\DashboardController', ['as' => 'admin']);

        Route::put('employee/{id}/verify', 'Admin\EmployeeController@verify')->name('admin.employee.verify');
        Route::put('employee/{id}/unverify', 'Admin\EmployeeController@unverify')->name('admin.employee.unverify');
        Route::resource('employee', 'Admin\EmployeeController', ['as' => 'admin']);

        Route::put('employer/{id}/verify', 'Admin\EmployerController@verify')->name('admin.employer.verify');
        Route::put('employer/{id}/unverify', 'Admin\EmployerController@unverify')->name('admin.employer.unverify');
        Route::resource('employer', 'Admin\EmployerController', ['as' => 'admin']);

    });

});

//test

Route::get('/print', 'PrintController@print');
