<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    @include('includes.head')

    <style type="text/css">

        html,
        body{
            height: 95%;
        }

        @media (max-width: 740px) {
        html,
        body{
            height: 100vh;
        }
        }

    </style>
    @stack('css')


</head>

<body class="white-skin">

    <!--Double navigation-->
    <header>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar navbar-light">
            <div class="container">
                @include('templates.navbar')
            </div>
        </nav>
        <!-- /.Navbar -->

        @yield('header')

    </header>
    <!--/.Double navigation-->

    @yield('content-top')

    <!--Main layout-->
    <main class="@hasSection('header') pt-5  @else mt-5 pt-5  @endif" id="app">

        <div class="container text-center pb-5">

            @yield('content')

        </div>

    </main>
    <!--/Main layout-->

    <!--Footer-->
    @include('templates.footer')
    <!--/.Footer-->

    @hasSection('vue')
        @yield('vue')
    @else
    <script>
        const app = new Vue({
            el: '#app'
        });
    </script>
    @endif

    <!-- SCRIPTS -->
    <script type="text/javascript" src="{{ asset('js/extra.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mdbootstrap-pro/js/mdb.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    <script>

        // SideNav Initialization
        $(".button-collapse").sideNav();

        new WOW().init();

        @foreach (['danger', 'error', 'warning', 'success', 'info'] as $type)
            @if(Session::has('alert-' . $type))
                toast('{{ $type }}', "{{ Session::get('alert-' . $type) }}");
            @endif
        @endforeach

    </script>

    @stack('scripts')

</body>

</html>
