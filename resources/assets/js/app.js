
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.swal = require('sweetalert2')
window.Vue = require('vue');
window.moment = require('moment');
import BootstrapVue from 'bootstrap-vue';
import StarRating from 'vue-star-rating';
import VueTimeago from 'vue-timeago'
Vue.component('star-rating', StarRating);
Vue.use(VueTimeago, { name: 'Timeago' })
Vue.use(BootstrapVue);