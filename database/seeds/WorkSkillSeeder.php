<?php

use Illuminate\Database\Seeder;

class WorkSkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('work_skills')->insert([
            ['name' => 'ironing clothes|menyeterika pakaian'],
            ['name' => 'doing laundry|membasuh pakaian'],
            ['name' => 'cooking|memasak'],
            ['name' => 'babysit a todler|menjaga bayi baru lahir'],
            ['name' => 'childcare|menyetrika kanak-kanak'],
            ['name' => 'elderly care or patient|menjaga orang tua atau orang sakit'],
            ['name' => 'teaching mengaji|mengajar mengaji'],
            ['name' => 'house cleaning|menbersih dan mengemas rumah'],
        ]);
    }
}
