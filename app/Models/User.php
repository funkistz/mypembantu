<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'type',
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function userable()
    {
        return $this->morphTo();
    }

    public function profilePictures()
    {
        return $this->hasMany(ProfilePicture::class);
    }

    public function avatar()
    {
        return $this->profilePictures()->where('is_primary', 1)->first();
    }

    public function getAvatarPathAttribute()
    {
        if (empty($this->avatar())) {
            return asset('img/profile/original.png');
        }

        $updated_at       = $this->avatar()->updated_at->timestamp;
        return $base_path = Storage::disk('local')->url($this->profilePictures()->where('is_primary', 1)->first()->path . '.png?' . $updated_at);
    }

    public function getAvatarPathMediumAttribute()
    {
        if (empty($this->avatar())) {
            return asset('img/profile/200.png');
        }

        $updated_at       = $this->avatar()->updated_at->timestamp;
        return $base_path = Storage::disk('local')->url($this->profilePictures()->where('is_primary', 1)->first()->path . '_200.png?' . $updated_at);
    }

    public function getAvatarPathThumbAttribute()
    {
        if (empty($this->avatar())) {
            return asset('img/profile/88.png');
        }

        $updated_at       = $this->avatar()->updated_at->timestamp;
        return $base_path = Storage::disk('local')->url($this->profilePictures()->where('is_primary', 1)->first()->path . '_88.png?' . $updated_at);
    }

    public function getIsVerifiedAttribute()
    {
        if (!empty($this->attributes['verified'])) {
            return true;
        } else {
            return false;
        }
    }

}
