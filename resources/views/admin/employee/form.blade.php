@extends('layouts.app')

@section('content')

<section>

<div class="row">
    <div class="col-md-8">
        <panel>
            <template slot="title">New Bus</template>
            <template slot="action">

            </template>
            <template slot="content">

                <!--begin::Form-->
                <form id="form-employee" action="{{ route('admin.employee.update', $employee->id) }}" method="post" >

                    <h4 class="card-title"><a>Personal Information</a></h4>
                    <hr>

                    @if(isset($method))
                    {{ method_field($method) }}
                    @endif
                    {{ csrf_field() }}

                    <b-form-group horizontal :label-cols="2" label="name">
                        <input name="name" type="text" class="form-control " placeholder="Name" value="{{ $employee->user->name or old('name') }}" >
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Display Name">
                        <input name="display_name" type="text" class="form-control " placeholder="Display Name" value="{{ $employee->display_name or old('display_name') }}" >
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Nationality">
                        <b-form-select name="nationality_id" v-bind:value="{{ $employee->nationality_id or old('nationality_id') }}" :options="{{ $nationalities }}" class="w-50"  >
                        </b-form-select>
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Race">
                        <b-form-select name="race_id" v-bind:value="{{ $employee->race_id or old('race_id') }}" :options="{{ $races }}" placeholder="hello" class="w-50"  >
                        </b-form-select>
                    </b-form-group>

                    <br>
                    <h4 class="card-title"><a>Address</a></h4>
                    <hr>

                    <b-form-group horizontal :label-cols="2" label="Line 1">
                        <input name="street" type="text" class="form-control " placeholder="Line 1" value="{{ $address->street or old('street') }}" >
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Line 2">
                        <input name="street_extra" type="text" class="form-control " placeholder="Line 2" value="{{ $address->street_extra or old('street_extra') }}" >
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Postcode">
                        <input name="post_code" type="text" class="form-control w-50" placeholder="Postcode" value="{{ $address->post_code or old('post_code') }}" >
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="City">
                        <input name="city" type="text" class="form-control " placeholder="City" value="{{ $address->city or old('city') }}" >
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="State">
                        <input type="text" class="form-control " placeholder="State" value="{{ $address->state or old('state') }}">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Country">
                        <b-form-select name="country_id" v-bind:value="{{ $address->country_id or old('country_id') }}" :options="{{ $countries }}" class="w-50"  >
                        </b-form-select>
                    </b-form-group>

                    <br>
                    <h4 class="card-title"><a>Social App</a></h4>
                    <hr>

                    <div class="md-form input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-sm btn-fb m-0" type="button"><i class="fa fa-facebook pr-1"></i></button>
                        </div>
                        <input name="facebook" type="text" class="form-control" placeholder="Facebook" value="{{ $employee->facebook or old('facebook') }}" >
                    </div>

                    <div class="md-form input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-sm btn-li m-0" type="button"><i class="fa fa-linkedin"></i></button>
                        </div>
                        <input name="linkedin" type="text" class="form-control" placeholder="Linkedin" value="{{ $employee->linkedin or old('linkedin') }}" >
                    </div>

                    <div class="md-form input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-sm btn-gplus m-0" type="button"><i class="fa fa-google-plus"></i></button>
                        </div>
                        <input name="google_plus" type="text" class="form-control" placeholder="Google Plus" value="{{ $employee->google_plus or old('google_plus') }}" >
                    </div>

                    <br>

                </form>

            </template>
            <template slot="footer">
                <a href="{{ route('admin.employee.index') }}" class="btn btn-flat">Cancel</a>
                <a onclick="$('#form-employee').submit();" class="btn btn-success">Submit</a>
            </template>

        </panel>
    </div>
</div>

</section>

@endsection

@section('vue')
<script defer>
$( document ).ready(function() {
    const app = new Vue({
        el: '#app',
    });
});

</script>
@endsection
