@extends('layouts.auth')

@section('nav-content')
<a class="btn btn-outline-pink btn-md my-2 my-sm-0 ml-3" href="{{ route('login') }}">Sign in</a>
@endsection

@section('content')

<div class="row justify-content-center">
    <div class="col-md-7">

        <div class="card card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">
            <div class="text-white text-center py-5 px-4 my-5">
                <div>
                    <h1 class="card-title pt-3 mb-5 font-bold"><strong>MyPembantu.com</strong></h1>
                    <p class="mx-5 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat,
                        laboriosam, voluptatem, optio vero odio nam sit officia accusamus
                        minus error nisi architecto nulla ipsum dignissimos. Odit sed qui,
                        dolorum!.</p>
                    <a class="btn btn-outline-white btn-rounded"><i class="fa fa-clone left"></i> View project</a>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-5">

        <!--Section: Live preview-->
        <section class="form-light mb-5">

            <!--Form without header-->
            <div class="card">

                <div class="card-body mx-4">

                    <!--Header-->
                    <div class="text-center">
                        <h3 class="pink-text mb-5"><strong>Sign Up</strong></h3>
                    </div>

                    <form action="/register" method="POST" id="form-register" >

                        {{ csrf_field() }}

                        <!--Body-->
                        <div class="md-form">
                            <input type="text" id="Form-name" class="form-control" name="name">
                            <label for="Form-name">Name</label>
                        </div>

                        <div class="md-form">
                            <input type="text" id="Form-email" class="form-control" name="email">
                            <label for="Form-email">Email</label>
                        </div>

                        <div class="md-form">
                            <input type="password" id="Form-pass" class="form-control" name="password">
                            <label for="Form-pass">Your password</label>
                        </div>

                        <div class="md-form pb-3">
                            <input type="password" id="Form-pass2" class="form-control" name="password_confirmation">
                            <label for="Form-pass2">Your password</label>
                        </div>

                        <!--Grid row-->
                        <div class="row d-flex align-items-center mb-4 mt-5">

                            <!--Grid column-->
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-pink btn-block btn-rounded z-depth-1">Sign up</button>
                            </div>
                            <!--Grid column-->

                        </div>
                        <!--Grid row-->

                    </form>

                </div>

                <!--Footer-->
                <div class="footer pt-3 unique-color-dark">

                    <ul class="list-unstyled list-inline text-center py-2">
                        <li class="list-inline-item">
                            <h5 class="mb-1 white-text">Already have an account?</h5>
                        </li>
                        <li class="list-inline-item">
                        <a href="{{ route('login') }} " class="btn btn-outline-white btn-rounded waves-effect waves-light">Sign in</a>
                        </li>
                    </ul>

                </div>


            </div>
            <!--/Form without header-->

        </section>
        <!--Section: Live preview-->

    </div>
</div>


@endsection

@push('scripts')

{!! JsValidator::formRequest('App\Http\Requests\RegisterRequest', '#form-register') !!}

@endpush
