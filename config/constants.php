<?php

return [
    'MARITAL_NEVER_MARRIED' => 1,
    'MARITAL_MARRIED'       => 2,
    'MARITAL_DIVORCED'      => 3,
    'MARITAL_SEPARATED'     => 4,
    'MARITAL_WIDOWED'       => 5,
];
