@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-3">

        @include('admin.navigation')

    </div>
    <div class="col-md-9">

        <!-- Card -->
        <div class="card">

        <!-- Card image -->
        <div class="view overlay">
        <a href="#!">
            <div class="mask rgba-white-slight"></div>
        </a>
        </div>

        <!-- Card content -->
        <div class="card-body">

        <!-- Title -->
        <h4 class="card-title">Dashboard</h4>

        </div>

        </div>
        <!-- Card -->

    </div>
</div>

@endsection

@section('vue')
<script defer>
    const app = new Vue({
        el: '#app',
        data: {
            edit: false,
            state: '{{ $address->state or null }}'
        },
        methods: {
            editMode: function () {
                this.edit = true;
            },
            viewMode: function () {

                var retVal = confirm("Do you want to quit editing? All your change with loss.");

                if( retVal == true ){
                        this.edit = false;
                }
            }
        }
    });

</script>
@endsection
