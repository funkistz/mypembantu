<?php

namespace App\Http\Controllers;

use Bnb\GoogleCloudPrint\Facades\GoogleCloudPrint;

class PrintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function print() {

        $printerId = '55605268-4840-5d77-d992-8db145bf0008';

        // Printing HTML from an URL
        GoogleCloudPrint::asHtml()
            ->url('https://opensource.org/licenses/MIT')
            ->printer($printerId)
            ->send();

    }

}
