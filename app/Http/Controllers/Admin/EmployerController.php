<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Employer;
use App\Models\Nationality;
use App\Models\Race;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employer::all();

        $data = [
            // 'breadcrumb' => [
            //     ['Admin', '#'],
            //     ['Employer', '#'],
            // ],
            'employers' => $employees,
        ];

        return view('admin.employer.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employer = Employer::find($id);

        $detail = [
            'name'         => $employer->user->name,
            'display name' => $employer->display_name,
            'height'       => $employer->height . 'cm',
            'weight'       => $employer->weight . 'kg',
            'nationality'  => $employer->nationality_name,
            'race'         => $employer->race_name,
        ];

        $data = [
            'employer' => $employer,
            'details'  => $detail,
        ];

        return view('admin.employer.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employer      = Employer::find($id);
        $countries     = Country::toJsonOption();
        $nationalities = Nationality::toJsonOption();
        $races         = Race::toJsonOption();
        $states        = State::toJsonOption('name', 'name');
        $address       = $employee->getPrimaryAddress();

        $data = [
            'breadcrumb'    => [
                ['Admin', route('admin.employer.index')],
                ['Employer', route('admin.employer.index')],
                ['Update', '#'],
            ],
            'action'        => route('admin.employer.update', $id),
            'method'        => 'PUT',
            'employer'      => $employer,
            'address'       => $address,
            'countries'     => $countries,
            'nationalities' => $nationalities,
            'races'         => $races,
            'states'        => $states,
        ];

        return view('admin.employer.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify($id)
    {
        $employer = Employer::find($id);
        $user     = $employer->user;

        $user->verified = Carbon::now();
        $user->save();

        return response()->json([
            'success' => 'Employer verified',
        ]);
    }

    public function unverify($id)
    {
        $employer = Employer::find($id);
        $user     = $employer->user;

        $user->verified = null;
        $user->save();

        return response()->json([
            'success' => 'Employer unverified',
        ]);
    }
}
