@extends('layouts.app2')

@push('css')
<style type="text/css">

  html,
  body,
  header,
  .view {
    height: 100%;
  }

  @media (max-width: 740px) {
    html,
    body,
    header,
    .view {
      height: 100vh;
    }
  }

</style>
@endpush

@section('header')

    @include('home.carousel')

@endsection

@section('content')

<!--Section: Main info-->
<section class="mt-5 wow fadeIn">

<!--Grid row-->
<div class="row">

  <!--Grid column-->
  <div class="col-md-6 mb-4">

    <img src="https://mdbootstrap.com/img/Marketing/mdb-press-pack/mdb-main.jpg" class="img-fluid z-depth-1-half" alt="">

  </div>
  <!--Grid column-->

  <!--Grid column-->
  <div class="col-md-6 mb-4">

    <!-- Main heading -->
    <h3 class="h3 mb-3">Material Design for Bootstrap</h3>
    <p>This template is created with Material Design for Bootstrap (
      <strong>MDB</strong> ) framework.</p>
    <p>Read details below to learn more about MDB.</p>
    <!-- Main heading -->

    <hr>

    <p>
      <strong>400+</strong> material UI elements,
      <strong>600+</strong> material icons,
      <strong>74</strong> CSS animations, SASS files, templates, tutorials and many more.
      <strong>Free for personal and commercial use.</strong>
    </p>

    <!-- CTA -->
    <a target="_blank" href="https://mdbootstrap.com/getting-started/" class="btn btn-grey btn-md">Download
      <i class="fa fa-download ml-1"></i>
    </a>
    <a target="_blank" href="https://mdbootstrap.com/components/" class="btn btn-grey btn-md">Live demo
      <i class="fa fa-image ml-1"></i>
    </a>

  </div>
  <!--Grid column-->

</div>
<!--Grid row-->

</section>
<!--Section: Main info-->

<hr class="my-5">

<!--Section: Main features & Quick Start-->
<section>

<h3 class="h3 text-center mb-5">About MDB</h3>

<!--Grid row-->
<div class="row wow fadeIn">

  <!--Grid column-->
  <div class="col-lg-6 col-md-12 px-4">

    <!--First row-->
    <div class="row">
      <div class="col-1 mr-3">
        <i class="fa fa-code fa-2x indigo-text"></i>
      </div>
      <div class="col-10">
        <h5 class="feature-title">Bootstrap 4</h5>
        <p class="grey-text">Thanks to MDB you can take advantage of all feature of newest Bootstrap 4.</p>
      </div>
    </div>
    <!--/First row-->

    <div style="height:30px"></div>

    <!--Second row-->
    <div class="row">
      <div class="col-1 mr-3">
        <i class="fa fa-book fa-2x blue-text"></i>
      </div>
      <div class="col-10">
        <h5 class="feature-title">Detailed documentation</h5>
        <p class="grey-text">We give you detailed user-friendly documentation at your disposal. It will help you to implement your ideas
          easily.
        </p>
      </div>
    </div>
    <!--/Second row-->

    <div style="height:30px"></div>

    <!--Third row-->
    <div class="row">
      <div class="col-1 mr-3">
        <i class="fa fa-graduation-cap fa-2x cyan-text"></i>
      </div>
      <div class="col-10">
        <h5 class="feature-title">Lots of tutorials</h5>
        <p class="grey-text">We care about the development of our users. We have prepared numerous tutorials, which allow you to learn
          how to use MDB as well as other technologies.</p>
      </div>
    </div>
    <!--/Third row-->

  </div>
  <!--/Grid column-->

  <!--Grid column-->
  <div class="col-lg-6 col-md-12">

    <p class="h5 text-center mb-4">Watch our "5 min Quick Start" tutorial</p>
    <div class="embed-responsive embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/cXTThxoywNQ" allowfullscreen></iframe>
    </div>
  </div>
  <!--/Grid column-->

</div>
<!--/Grid row-->

</section>
<!--Section: Main features & Quick Start-->

@endsection

@section('vue')
<script defer>
    const app = new Vue({
        el: '#app',
        data: {
            items: [{id:1}, {id:2}],
        },
        methods: {
        }
    });

</script>
@endsection
