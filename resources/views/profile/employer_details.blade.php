@extends('layouts.app2')

@section('content')

@include('employer.alert')

<div class="row">
    <div class="col-md-4">

        @include('profile.profile_card')

    </div>
    <div class="col-md-8">

        <div class="card" style="min-height:100%;">

            <!-- Card content -->
            <div class="card-body text-left">

                <!--begin::Form-->
                <form id="form-profile" action="{{ route('employer.personal.store') }}" method="post" v-cloak>

                    <!-- Title -->
                    <button type="button" class="btn btn-sm btn-indigo mt-0 float-right" v-on:click="editMode" v-if="!edit">Edit</button>
                    <button type="button" class="btn btn-sm btn-blue-grey mt-0 float-right" v-on:click="viewMode" v-if="edit">Cancel</button>
                    <button type="submit" class="btn btn-sm btn-primary mt-0 float-right" v-if="edit">Save</button>
                    <h4 class="card-title"><a>Personal Information</a></h4>
                    <hr>

                    @if(isset($method))
                    {{ method_field($method) }}
                    @endif
                    {{ csrf_field() }}

                    <b-form-group horizontal :label-cols="2" label="Name">
                        <input name="name" type="text" class="form-control " placeholder="Name" value="{{ $user->name or old('name') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Display Name">
                        <input name="display_name" type="text" class="form-control " placeholder="Display Name" value="{{ $employer->display_name or old('display_name') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Nationality">
                        <b-form-select name="nationality_id" v-bind:value="{{ !empty($employer->nationality_id)? $employer->nationality_id:0  }}" :options="{{ $nationalities }}" class="w-50" :readonly="!edit" :disabled="!edit">
                        </b-form-select>
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Race">
                        <b-form-select name="race_id" v-bind:value="{{ !empty($employer->race_id)? $employer->race_id:0 }}" :options="{{ $races }}" class="w-50" :readonly="!edit" :disabled="!edit">
                        </b-form-select>
                    </b-form-group>

                    <br>
                    <h4 class="card-title"><a>Address</a></h4>
                    <hr>

                    <b-form-group horizontal :label-cols="2" label="Line 1">
                        <input name="street" type="text" class="form-control " placeholder="Line 1" value="{{ $address->street or old('street') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Line 2">
                        <input name="street_extra" type="text" class="form-control " placeholder="Line 2" value="{{ $address->street_extra or old('street_extra') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Postcode">
                        <input name="post_code" type="text" class="form-control w-50" placeholder="Postcode" value="{{ $address->post_code or old('post_code') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="City">
                        <input name="city" type="text" class="form-control " placeholder="City" value="{{ $address->city or old('city') }}" :readonly="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="State">
                        <b-form-select name="state" v-model="state" :options="{{ $states }}" class="w-50" :readonly="!edit" :disabled="!edit" v-if="edit">
                        </b-form-select>
                        <input type="text" class="form-control " placeholder="State" readonly v-model="state"  v-if="!edit">
                    </b-form-group>

                    <b-form-group horizontal :label-cols="2" label="Country">
                        <b-form-select name="country_id" v-bind:value="{{ !empty($address->country_id)? $address->country_id:0 }}" :options="{{ $countries }}" class="w-50" :readonly="!edit" :disabled="!edit">
                        </b-form-select>
                    </b-form-group>

                    <br>
                    <h4 class="card-title"><a>Social App</a></h4>
                    <hr>

                    <div class="md-form input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-sm btn-fb m-0" type="button"><i class="fa fa-facebook pr-1"></i></button>
                        </div>
                        <input name="facebook" type="text" class="form-control" placeholder="Facebook" value="{{ $employer->facebook or old('facebook') }}" :readonly="!edit">
                    </div>

                    <div class="md-form input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-sm btn-li m-0" type="button"><i class="fa fa-linkedin"></i></button>
                        </div>
                        <input name="linkedin" type="text" class="form-control" placeholder="Linkedin" value="{{ $employer->linkedin or old('linkedin') }}" :readonly="!edit">
                    </div>

                    <div class="md-form input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-sm btn-gplus m-0" type="button"><i class="fa fa-google-plus"></i></button>
                        </div>
                        <input name="google_plus" type="text" class="form-control" placeholder="Google Plus" value="{{ $employer->google_plus or old('google_plus') }}" :readonly="!edit">
                    </div>

                    <br>

                    <b-form-group horizontal :label-cols="2" label="">
                        <button type="button" class="btn btn-sm btn-blue-grey mt-0 float-right" v-on:click="viewMode" v-if="edit">Cancel</button>
                        <button class="btn btn-sm btn-primary m-0 float-right" v-if="edit">Save</button>
                    </b-form-group>


                </form>

            </div>

        </div>

    </div>
</div>


@endsection

@section('vue')
<script defer>
    const app = new Vue({
        el: '#app',
        data: {
            edit: false,
            state: '{{ $address->state or null }}'
        },
        methods: {
            editMode: function () {
                this.edit = true;
            },
            viewMode: function () {

                var retVal = confirm("Do you want to quit editing? All your change with loss.");

                if( retVal == true ){
                        this.edit = false;
                }
            }
        }
    });

</script>
@endsection

@push('scripts')
{!! JsValidator::formRequest('App\Http\Requests\EmployerPersonalRequest', '#form-profile') !!}
<script>

</script>
@endpush
