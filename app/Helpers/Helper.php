<?php

if (!function_exists('toJsonOption')) {

    function toJsonOption($colletion, $name = 'name', $value = 'id')
    {

        return $colletion->mapWithKeys(function ($item) use ($name, $value) {
            return [
                'name'  => $item[$name],
                'value' => $item[$value],
            ];
        })->all();
    }
}
