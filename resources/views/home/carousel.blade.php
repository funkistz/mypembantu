<!-- <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">

<ol class="carousel-indicators">
    <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
</ol>

<div class="carousel-inner" role="listbox">

    <div class="carousel-item active">
    <div class="view" style="background-image: url('https://mdbootstrap.com/img/Photos/Others/nature7.jpg'); background-repeat: no-repeat; background-size: cover; height:60vh;">

        <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

        <div class="text-center white-text mx-5 wow fadeIn">
            <h1 class="mb-4">
            <strong>Selamat datang ke MyPembantu.com</strong>
            </h1>

            <p>
            <strong>
                Kami menyediakan pusat sehenti bagi anda mencari atau menjadi pembantu rumah <br>
                secara sepenuh masa atau sambilan dan tinggal di rumah majikan atau berulang-alik setiap hari.
            </strong>
            </p>

            <p class="mb-4 d-none d-md-block">
            <strong>Jadi, boleh kami bantu anda?</strong>
            </p>

            <a href="{{ route('register.employee.create') }}" class="btn btn-outline-white btn-lg">Ya, Saya sedang mencari pembantu rumah</a>
            <a href="{{ route('register.employee.create') }}" class="btn btn-outline-white btn-lg">Ya, Saya ingin memohon menjadi pembantu rumah</a>
        </div>

        </div>

    </div>
    </div>

</div>

<a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>

</div> -->

<!-- Full Page Intro -->
<div class="view  jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('https://mdbootstrap.com/img/Photos/Others/gradient1.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
      <!-- Mask & flexbox options-->
      <div class="mask rgba-purple-slight d-flex justify-content-center align-items-center">
        <!-- Content -->
        <div class="container">
          <!--Grid row-->
          <div class="row wow fadeIn">
            <!--Grid column-->
            <div class="col-md-12 text-center">
              <h1 class="display-4 font-weight-bold mb-0 pt-md-5 pt-5 wow fadeInUp">Selamat datang ke MyPembantu.com</h1>
              <h5 class="pt-md-5 pt-sm-2 pt-5 pb-md-5 pb-sm-3 pb-5 wow fadeInUp" data-wow-delay="0.2s">
                Kami menyediakan pusat sehenti bagi anda mencari atau menjadi pembantu rumah <br>
                secara sepenuh masa atau sambilan dan tinggal di rumah majikan atau berulang-alik setiap hari.
              </h5>
              <div class="wow fadeInUp" data-wow-delay="0.4s">
                <a href="{{ route('register.employer.create') }}" class="btn btn-light-blue btn-lg waves-effect waves-light">Ya, Saya sedang mencari pembantu rumah</a>
                <a href="{{ route('employee.search.index') }}" class="btn btn-indigo btn-lg waves-effect waves-light">Ya, Saya ingin memohon menjadi pembantu rumah</a>
              </div>
            </div>
            <!--Grid column-->
          </div>
          <!--Grid row-->
        </div>
        <!-- Content -->
      </div>
      <!-- Mask & flexbox options-->
    </div>
    <!-- Full Page Intro -->
