<?php

namespace app\Libraries;

use App\Models\FileFolder;
use App\Repositories\Learning\FileFolderRepository;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class FileFolderLibrary
{

    public function __construct(
        FileFolderRepository $file_folder
    ) {
        $this->file_folder = $file_folder;
    }

    public static function upload($name, $path, $file)
    {
        $path          = preg_replace('{/$}', '', $path);
        $original_path = $path;

        //add tenant folder as parent folder
        $path = config('b3.tenant_path') . '/' . $path;

        if (pathinfo($name, PATHINFO_EXTENSION) == "") {
            $extension = $file->getClientOriginalExtension(); // getting file extension
            $fileName  = $name . '.' . $extension;
        } else {
            $fileName = $name;
        }

        $full_path = $path . '/' . $fileName;

        if (is_string($file)) {
            $upload_path = Storage::disk(config('b3.disk'))->put($full_path, $file);
        } else {
            $upload_path = Storage::disk(config('b3.disk'))->putFileAs($path, $file, $fileName);
        }

        $data['name'] = $fileName;
        $data['path'] = $original_path . '/' . $fileName;

        return $data;
    }

    public static function move($file, $move_path)
    {
        $file_path    = preg_replace('{/$}', '', $file->path);
        $move_path    = preg_replace('{/$}', '', $move_path);
        $db_move_path = $move_path . '/' . $file->name;

        //add tenant folder as parent folder
        $file_path = config('b3.tenant_path') . '/' . $file_path;
        $move_path = config('b3.tenant_path') . '/' . $db_move_path;

        Storage::disk(config('b3.disk'))->move($file_path, $move_path);

        return $db_move_path;

    }

    public static function uploadImage($name, $path, $file, $option = [])
    {

        if (!FileFolderLibrary::allowedImageType($file)) {
            return false;
        }

        $img      = str_replace('data:image/png;base64,', '', $file);
        $img      = str_replace(' ', '+', $img);
        $img_data = base64_decode($img);

        $image = Image::make($img_data);

        if (!empty($option['blur'])) {
            if ($option['blur']) {
                $image->blur($data['blur']);
            }
        }

        $image_normal = $image;

        if (!empty($option['ratio'])) {
            $image_normal->resize($option['ratio'][0], $option['ratio'][1], function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $image_normal->encode(config('b3.image_ext'));
        $image_normal = $image_normal->stream();
        $ext          = '.' . config('b3.image_ext');

        $upload = FileFolderLibrary::upload($name . $ext, $path, $image_normal->__toString());

        $image2 = Image::make($img_data);

        $image_compressed = $image2->resize(400, 400,
            function ($constraint) {
                $constraint->aspectRatio();
            })
            ->resizeCanvas(400, 400)->stream();

        FileFolderLibrary::upload($name . '_compressed' . $ext, $path, $image_compressed->__toString());

        if (!empty($option['thumb'])) {
            if ($option['thumb']) {
                $image_medium = $image->fit(config('b3.image_medium'))->encode(config('b3.image_ext'));
                $image_medium = $image_medium->stream();
                $medium_size  = '_' . config('b3.image_medium');

                FileFolderLibrary::upload($name . $medium_size . $ext, $path, $image_medium->__toString());

                $image_thumb = $image->fit(config('b3.image_thumb'))->encode(config('b3.image_ext'));
                $image_thumb = $image_thumb->stream();
                $thumb_size  = '_' . config('b3.image_thumb');

                FileFolderLibrary::upload($name . $thumb_size . $ext, $path, $image_thumb->__toString());
            }

        }

        $data['name'] = $name;
        $data['path'] = $path . $name . $ext;

        return $data;
    }

    public static function allowedImageType($file)
    {
        $allowedMimeTypes = config('b3.allowed_mime_type');

        if (!is_string($file)) {
            $contentType = $file->getClientMimeType();
        } else {
            $pos         = strpos($file, ';');
            $contentType = explode(':', substr($file, 0, $pos))[1];
        }

        if (!in_array($contentType, $allowedMimeTypes)) {
            return false;
        }

        return true;
    }

    public static function deleteFile($path)
    {

        $path = config('b3.tenant_path') . '/' . $path;
        return Storage::disk(config('b3.disk'))->delete($path);
    }

    //work only with filefolder model
    public static function deleteFolder(FileFolder $folder)
    {

        $file_folder_repository = new FileFolderRepository(app());
        $nodes                  = FileFolder::descendantsAndSelf($folder->id)->toTree();

        FileFolderLibrary::deleteFile($folder->path);
        $file_folder_repository->delete($folder->id);

        $traverse = function ($file_folders) use (&$traverse, $file_folder_repository) {
            foreach ($file_folders as $file_folder) {

                if ($file_folder->type == FILEFOLDER_TYPE_FILE) {
                    FileFolderLibrary::deleteFile($file_folder->path);
                }

                $traverse($file_folder->children);
            }
        };

        $traverse($nodes);

        return true;
    }
}
