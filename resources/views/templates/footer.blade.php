<footer class="page-footer text-center text-md-left pt-4 unique-color-dark">

    <!--Copyright-->
    <div class="footer-copyright py-3 text-center">
        <div class="container-fluid">
            © 2018 Copyright: <a href="http://www.mypembantu.com"> MyPembantu.com </a>

        </div>
    </div>
    <!--/.Copyright-->

</footer>
