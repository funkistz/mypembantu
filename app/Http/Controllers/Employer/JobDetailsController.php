<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use App\Models\WorkSkill;
use Illuminate\Http\Request;

class JobDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user                 = auth()->user();
        $employee             = $user->userable;
        $employee_work_skills = $employee->workSkills->map(function ($item, $key) {
            return $item->id;
        })->toArray();
        $work_skills = WorkSkill::all();

        // dd($user->userable->workSkills->map(function ($item, $key) {
        //     return $item->id;
        // }));

        $data = [
            'user'                 => $user,
            'employer'             => $employee,
            'employee_work_skills' => $employee_work_skills,
            'work_skills'          => $work_skills,
        ];

        return view('employer.job_details')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user     = auth()->user();
        $employer = $user->userable;

        if (!empty($request['skills'])) {
            $request['skills'] = array_keys($request['skills']);
        } else {
            $request['skills'] = [];
        }

        // dd($request->all());

        $employer->update(
            [
                'about'          => $request['about'],
                'location_type'  => $request['location_type'],
                'households'     => $request['households'],
                'employee_types' => $request['employee_types'],

                'salary_monthly' => $request['salary_monthly'],
                'salary_daily'   => $request['salary_daily'],
                'salary_hourly'  => $request['salary_hourly'],
            ]
        );

        $employer->workSkills()->detach();
        $employer->workSkills()->attach($request['skills']);

        \Session::flash('alert-success', 'Employer Related Information Updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
