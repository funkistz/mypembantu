@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-3">

        @include('admin.navigation')

    </div>
    <div class="col-md-9">

        <panel>
            <template slot="title">Maid List</template>
            <template slot="action">

            </template>
            <template slot="table">
                <div class="table-responsive">

                    <datatable>
                        <template slot="header">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Register At</th>
                                <th>Verification</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </template>
                        <template slot="body">
                            @foreach($employees as $employee)
                            <tr>
                                <td>
                                    <a href="{{ route('admin.employee.show', $employee->id) }}">{{ !empty($employee->user)? $employee->user->name:'' }}</a>
                                </td>
                                <td>{{ !empty($employee->user)? $employee->user->email:'' }}</td>
                                <td>{{ $employee->created_at->toFormattedDateString() }}</td>

                                @if( !empty($employee->user) )
                                    @if( !empty($employee->user->verified) )
                                    <td>Verified</td>
                                    @else
                                    <td>
                                        <a href="{{ route('admin.employee.show', $employee->id) }}" class="btn btn-rounded btn-sm btn-outline-success">
                                            Verify
                                        </a>
                                    </td>
                                    @endif
                                @else
                                <td></td>
                                @endif

                                <!-- <td nowrap>
                                    <btn-icon action="update" href="{{ route('admin.employee.edit', $employee->id) }}" v-cloak></btn-icon>

                                    <btn-icon action="delete" href="{{ route('admin.employee.destroy', $employee->id) }}" confirm="true" ajax="DELETE" reload="true" v-cloak></btn-icon>
                                </td> -->
                            </tr>
                            @endforeach
                        </template>
                    </datatable>

                </div>
            </template>
            {{-- <template slot="footer">
                <h4>Footer here</h4>
            </template> --}}

        </panel>
    </div>
</div>

@endsection

@section('vue')
<script defer>
$( document ).ready(function() {
    const app = new Vue({
        el: '#app',
    });
});

</script>
@endsection

@push('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>

</script>

@endpush
