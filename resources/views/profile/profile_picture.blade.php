@extends('layouts.app2')

@section('content')

<div class="row">
    <div class="col-md-4">

        @include('profile.profile_card')

    </div>
    <div class="col-md-8">

        <div class="card p-5" style="min-height:100%;">

            <div class="row">

                <div class="col-md-1"></div>
                <div class="col-md-10">

                    <!-- Card Narrower -->
                    <div class="card card-cascade narrower">

                        <!-- Card image -->
                        <div class="">
                        <div id="demo" style="height:300px;"></div>
                            <!-- <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(147).jpg" alt="Card image cap"> -->
                        </div>

                        <div class="card-body card-body-cascade">

                            <h4 class="card-title mt-5">Choose image</h4>

                            <form class="md-form">
                                <div class="file-field">
                                    <div class="btn btn-deep-purple btn-sm float-left">
                                        <span>Choose file</span>
                                        <input id="file-picker" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload your file">
                                    </div>
                                </div>
                            </form>

                            <br>

                            <div id="upload-loader" class="progress primary-color-dark d-none">
                                <div class="indeterminate"></div>
                            </div>

                            <b-form-group horizontal :label-cols="2" label="">
                                <button class="btn btn-sm btn-primary m-0 float-right" id="upload-btn">Save</button>
                            </b-form-group>

                        </div>

                    </div>
                    <!-- Card Narrower -->

                </div>
                <div class="col-md-1"></div>

            </div>

        </div>

    </div>
</div>


@endsection

@section('vue')
<script defer>

    const app = new Vue({
        el: '#app',
    });

</script>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/Croppie/croppie.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/Croppie/croppie.css') }}">

<script>

$(document).ready(function(){

    var basic =

    $('#demo').croppie({
        viewport: {
            width: 200,
            height: 200
        }
    });

    basic.croppie('bind', {
        url: '{{ $user->avatarPath }}',
        zoom: 0
    });

    $('#file-picker').change(function() {

        readURL(this);
    });

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
            // $('#blah').attr('src', e.target.result);

                basic.croppie('destroy');

                basic = $('#demo').croppie({
                    viewport: {
                        width: 200,
                        height: 200
                    }
                });

                basic.croppie('bind', {
                    url: e.target.result,
                    zoom: 0
                });

            }

            reader.readAsDataURL(input.files[0]);
        }


    }

    function upload(){

        basic.croppie('result', 'base64').then(function(blob) {

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('blob', blob);

            //loading
            $('#upload-loader').removeClass('d-none');
            $('#upload-btn').attr('disabled', true);

            $.ajax({
                url: "{{ route('profile_picture.store') }}",
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: formData
            }).done(function(response)
            {
                toast(response.status, response.message);
                $('#upload-loader').addClass('d-none');
                $('#upload-btn').attr('disabled', false);

                window.location.reload();
            });
        });

    }

    $('#upload-btn').on('click', function(){

        upload();

    });

});

</script>
@endpush
