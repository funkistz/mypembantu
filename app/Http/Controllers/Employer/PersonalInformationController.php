<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployerPersonalRequest;
use App\Models\Country;
use App\Models\Employer;
use App\Models\Nationality;
use App\Models\Race;
use App\Models\State;
use Illuminate\Http\Request;

class PersonalInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user     = auth()->user();
        $employer = $user->userable;

        if (empty($employer)) {
            $employer                       = new Employer();
            $employer->is_profile_completed = false;
            $employer->save();

            $user->userable_id   = $employer->id;
            $user->userable_type = Employer::class;
            $user->save();
        }

        $address = $employer->getPrimaryAddress();

        $countries      = Country::toJsonOption();
        $nationalities  = Nationality::toJsonOption();
        $races          = Race::toJsonOption();
        $states         = State::toJsonOption('name', 'name');
        $marital_status = json_encode([
            ['text' => 'Never married', 'value' => 1],
            ['text' => 'Married', 'value' => 2],
            ['text' => 'Divorced', 'value' => 3],
            ['text' => 'Separated', 'value' => 4],
            ['text' => 'Widowed', 'value' => 5],
        ]);

        $data = [
            'user'           => $user,
            'employer'       => $employer,
            'address'        => $address,
            'countries'      => $countries,
            'nationalities'  => $nationalities,
            'races'          => $races,
            'states'         => $states,
            'marital_status' => $marital_status,
        ];

        return view('profile.employer_details')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(EmployerPersonalRequest $request)
    {
        // dd($request->all());

        $user     = auth()->user();
        $employer = $user->userable;

        $user->update(
            [
                'name' => $request['name'],
            ]
        );

        $employer->update(
            [
                'display_name'   => $request['display_name'],
                'nationality_id' => $request['nationality_id'],
                'race_id'        => $request['race_id'],

                'facebook'       => $request['facebook'],
                'linkedin'       => $request['linkedin'],
                'google_plus'    => $request['google_plus'],
            ]
        );

        $address = [
            'street'       => $request['street'],
            'street_extra' => $request['street_extra'],
            'state'        => $request['state'],
            'city'         => $request['city'],
            'post_code'    => $request['post_code'],
            'country_id'   => $request['country_id'],
            'is_primary'   => true,
        ];

        if ($employer->hasAddress()) {
            $current_address = $employer->addresses()->first();
            $employer->updateAddress($current_address, $address);
        } else {
            $employer->addAddress($address);
        }

        $employer->is_profile_completed = 1;
        $employer->save();

        \Session::flash('alert-success', 'Personal Information Updated');

        return redirect()->back();
    }
}
