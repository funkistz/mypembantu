<?php

namespace App\Models;

use App\Traits\toJsonOption;
use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
    use toJsonOption;

    protected $fillable =
    array(
        'id',
        'name',
    );
    public $timestamps = false;
}
